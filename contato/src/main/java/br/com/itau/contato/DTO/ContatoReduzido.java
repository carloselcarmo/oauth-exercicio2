package br.com.itau.contato.DTO;

import br.com.itau.contato.model.Contato;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ContatoReduzido
{
    @NotNull(message =  "Nome não pode ser nulo")
    @NotBlank(message =  "Nome não pode ser branco")
    private String nome;

    @NotNull(message =  "Email não pode ser nulo")
    @NotBlank(message =  "Email não pode ser branco")
    @Email(message = "Email inválido")
    private String email;

    @NotNull(message =  "Telefone não pode ser nulo")
    @NotBlank(message =  "Telefone não pode ser branco")
    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public ContatoReduzido() {
    }

    public ContatoReduzido(Contato contato)
    {
        this.setEmail(contato.getEmail());
        this.setNome(contato.getNome());
        this.setTelefone(contato.getTelefone());
    }
}
