package br.com.itau.contato.controller;

import br.com.itau.contato.DTO.ContatoReduzido;
import br.com.itau.contato.model.Contato;
import br.com.itau.contato.security.Usuario;
import br.com.itau.contato.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatoController
{
    @Autowired
    private ContatoService contatoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContatoReduzido criar(@RequestBody @Valid ContatoReduzido contatoReduzido, @AuthenticationPrincipal Usuario usuario)
    {
        return contatoService.criar(contatoReduzido, usuario);
    }

    @GetMapping
    public List<ContatoReduzido> buscar(@AuthenticationPrincipal Usuario usuario)
    {
        return contatoService.buscar(usuario);
    }
}
