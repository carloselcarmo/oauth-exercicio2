package br.com.itau.contato.service;

import br.com.itau.contato.DTO.ContatoReduzido;
import br.com.itau.contato.model.Contato;
import br.com.itau.contato.repository.ContatoRepository;
import br.com.itau.contato.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService
{
    @Autowired
    ContatoRepository contatoRepository;

    public ContatoReduzido criar(ContatoReduzido contatoReduzido, Usuario usuario)
    {
        Contato contato = new Contato();
        contato.setEmail(contatoReduzido.getEmail());
        contato.setNome(contatoReduzido.getNome());
        contato.setTelefone(contatoReduzido.getTelefone());
        contato.setUsuario(usuario.getName());

        return  new ContatoReduzido(contatoRepository.save(contato));
    }

    public List<ContatoReduzido> buscar(Usuario usuario)
    {
        List<Contato> contatos = contatoRepository.findByUsuario(usuario.getName());
        List<ContatoReduzido> contatosReduzidos = new ArrayList<>();
        for(Contato contato : contatos)
        {
            contatosReduzidos.add(new ContatoReduzido(contato));
        }

        return contatosReduzidos;
    }
}
