package br.com.itau.contato.repository;

import br.com.itau.contato.model.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContatoRepository extends JpaRepository<Contato,Integer >
{
    List<Contato> findByUsuario(String usuario);
}