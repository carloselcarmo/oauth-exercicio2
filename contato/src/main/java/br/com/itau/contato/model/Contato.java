package br.com.itau.contato.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Contato
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message =  "Nome não pode ser nulo")
    @NotBlank(message =  "Nome não pode ser branco")
    private String nome;

    @NotNull(message =  "Email não pode ser nulo")
    @NotBlank(message =  "Email não pode ser branco")
    @Email(message = "Email inválido")
    private String email;

    @NotNull(message =  "Telefone não pode ser nulo")
    @NotBlank(message =  "Telefone não pode ser branco")
    private String telefone;

    private String usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Contato() {
    }
}
